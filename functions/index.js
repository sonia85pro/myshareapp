const functions = require('firebase-functions');

const app = require('express')();


const Config = {
    apiKey: "AIzaSyBdlBj-tS1Cf4PEB6I9s_eOCiQvc5xTcdU",
    authDomain: "masharecreche.firebaseapp.com",
    databaseURL: "https://masharecreche.firebaseio.com",
    projectId: "masharecreche",
    storageBucket: "masharecreche.appspot.com",
    messagingSenderId: "409952193451",
    appId: "1:409952193451:web:9b5d5bc3506c13892f20ee",
    measurementId: "G-ZS1VMN8KZK"
};


const firebase = require ('firebase');
firebase.initializeApp(Config);



app.get('/screams', (req,res) => {
   db
    .collection('screams')
    .orderBy('createdAt','desc')
    .get()
    .then((data) => {
        let screams = [];
        data.forEach((doc) => {
            screams.push({
                screamId : doc.id,
                body: doc.data().body,
               userHandle: doc.data().userHandle,
               createdAt: doc.data().createdAt   
            });
        });
        return res.json(screams);
    })
    .catch((err)=> console.error(err));
});
const FBAuth = (req, res,next) => {
    let idToken;
    if(req.headers.authorization && req.headers.authorization.startswith('Bearer')){
        idToken = req.headers.authorization.split('Bearer')[1];
    } else{
        return res.status(403).json({ error:'Unauthorized'});
    }

admin.auth().verifyIdToken(idToken)
.then(decodeToken => {
    req.user = decodeToken;
    console.log(decodedToken);
    return db.collection('users')
    .where('userId', '==', req.user.uid)
    .limit(1)
    .get();
})
.then(data => {
    req.user.handle =data.docs[0].data().handle;
    return next();
})
.catch(err => {
    console.error('Error while verifying token', err);
    return res.status(403).json(err);
});
};


//Post one scream
app.post('/scream', FBAuth, (req, res) => {
    if (req.body.body.trim() === '') {
        return res.status(400).json({ body: 'Body must not be empty'});
    }
   
    const newScream = {
        body: req.body.body,
        userHandle: req.body.handle,
        createdAt: new Date().toISOString()
    };

    db.collection('screams')
    .add(newScream)
    .then((doc) => {
     res.json({ message: `document ${doc.id} created successfully` });
    })
    .catch((err) => {
        res.status(500).json({ error: 'something went wrong'});
        console.error(err);
    });

 });
 //verification que l email is not vide
 const isEmail = (email) => {
     const regEx =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(email.match(regEx)) return true;
      else return false;
    };

//Post one scream
const isEmpty = (string) => {
  if (string.trim() === '') return true;
  else return false;
};

//signup route
app.post('/signup', (req, res) => {
    const newUser = {
        email : req.body.email,
        password : req.body.password,
        confirmPassword : req.body.confirmPassword,
        handle : req.body.handle
    };
    let errors = {};

    if(isEmpty(newUser.email)) {
errors.email = 'Must not be empty';
    } else if(!isEmail(newUser.email)){
        errors.email = 'Must be a valid email address';

    }
if(isEmpty(newUser.password)) errors.password = 'Must not be empty';
if(newUser.password !== newUser.confirmPassword) 
errors.confirmPassword = 'el mot de pass ghalete hhh';
if (isEmpty(newUser.handle)) errors.handle = "Must not be empty"; 

if(Object.keys(errors).length > 0) return res.status(400).json(errors);

//TOOO: validate date
    let token, userId;
    db.doc(`/users/${newUser.handle}`)
    .get()
    .then((doc) => {
        if (doc.exists){
            return res.status(400).json({ handle:'this handle is already taken'});
        } else {
            return firebase
               .auth()
               .createUserWithEmailAndPassword(newUser.email, newUser.password);
        }
    })
    .then((data) => {
        userId = data.user.uid;
       return data.user.getIdToken();
    })
    
    .then((idToken) => {
        token = idToken;
        const userCredentials = {
            handle: newUser.handle,
            email: newUser.email,
            createAt: new Date().toISOString(),
            userId
        };
         return db.doc(`/users/${newUser.handle}`).set(userCredentials);
       
    })
    .then(() => {
      return res.status(201).json({ token});
    })
    .catch((err)  => {
        console.error(err);
        if (err.code === 'auth/email-already-in-use') {
            return res.status(400).json({ email:'Email is already in use'});
        } else {
                 return res.status(500).json({ error: err.code });
               }
    });

 
});

app.post('/login', (req, res) => {
    const user = { 
        email: req.body.email,
        password: req.body.password
    };

    let errors = {};
  
    if(isEmpty(user.email)) errors.email = 'Must not be empty';
    if(isEmpty(user.password)) errors.password = 'Must not be empty';
    if(Object.keys(errors).length > 0 ) return res.status(400).json(errors);
 
 
    firebase
 .auth()
 .signInWithEmailAndPassword(user.email, user.password)
 .then((data) => {
     return data.user.getIdToken();
 })
 .then((token) => {
     return res.json({token});

 })
 .catch((err) => {
     console.error(err);
     if (err.code === 'auth/wrong-password'){
         return  res.status(403).json({ general: 'wrong creadentials, pleas try again'});
     } else
      return res.status(500).json({ error: err.code });
 });
});
app.post('/login',(req,res) =>{
    const user ={
      email: req.body.email,
      password: req.body.password  
    };
    let errors ={};
    if(isEmpty(user.email)) errors.email ='Must not be empty';
     if (isEmpty(user.password)) errors.password = "Must not be empty";
     if(Object.keys(errors).length > 0) return res.status(400).json(errors);
     
     firebase 
     .auth()
     .signInWithEmailAndPassword(user.email, user.password).then(data =>{
        return data.user.getIdToken(); 
     })
     .then((token) => {
         return res.json ({token});
     })
     .catch((err) => {
         console.error(err);
         if( err.code === 'auth/wrong-password'){
             return res.status(403).json({ general:'wrong credentials, please try again'});

         } else
         return res.status(500).json({ error:err.code});
     });

});
 exports.api = functions.region('europe-west1').https.onRequest(app);